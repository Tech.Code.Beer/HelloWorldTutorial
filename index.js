const express = require('express');
const app = express();

app.get('/', function (req, res) {
  res.send('TechCodeBeer... Life in our Digital World!');
});

app.get('/anotherRoute', function (req, res) {
  res.send('This is another response to a different route.');
  });

app.listen(3001, function () {
  console.log('Example app listening on port 3001!')
});
